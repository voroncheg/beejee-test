<?php


namespace Models;

use System\Model;

class Users extends Model
{
    public function checkUser($data){
        $this->sql = 'SELECT * FROM users WHERE name = :name AND password = :password';
        $statement = $this->pdo->prepare($this->sql);
        $statement->execute([':name' => $data['name'], ':password' => $data['password']]);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }
}