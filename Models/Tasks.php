<?php

namespace Models;

use System\Model;

class Tasks extends Model
{
    
    public function displayAll($get){

        $order_param = 'id';
        $order_type = 'DESC';
        $is_default = true;

        if(!empty($get['name'])){
            $is_default = false;
            $order_param = 'name';
            $order_type = $get['name'];
        }

        if(!empty($get['email'])){
            $is_default = false;
            $order_param = 'email';
            $order_type = $get['email'];
        }

        $page = (int) $get['page'];
        if(empty($page)){
            $page = 1;
        }

    	$perPage = 3;
    	$start = $perPage * ($page - 1);
    	$total = (int) $this->pdo->query('SELECT COUNT(*) FROM tasks')->fetchColumn();
    	$totalPages = (int) ceil($total / $perPage);

    	$next = $page+1;
        $prev = $page-1;

        if(empty($get['task_done'])){
            $this->sql = "SELECT * FROM tasks ORDER BY $order_param $order_type LIMIT $start, $perPage";
        }else{
            $is_default = false;
            $order_param = 'task_done';
            $order_type = $get['task_done'];
            $this->sql = "SELECT * FROM tasks WHERE task_done = 1 ORDER by id $order_type LIMIT $start, $perPage";
        }

        if($is_default){
            $ordered_params = '';
        }else{
            $ordered_params = '&'.$order_param.'='.$order_type;
        }

        $paginatoinInfo = [
            "page"              => $page,
            "start"             => $start,
            "totalPages"        => $totalPages,
            "next"              => $next,
            "prev"              => $prev,
            "ordered_params"    => $ordered_params
        ];

        $res = [];
        $res['result'] = $this->pdo->query($this->sql)->fetchAll(\PDO::FETCH_ASSOC);

        $res['pagination'] = $paginatoinInfo;
        
        return $res;
    }

    public function insertTask($data){

        $data['tasktext'] = htmlspecialchars($data['tasktext']);

        $this->sql = 'INSERT INTO tasks (name, email, tasktext) VALUES(:name, :email, :tasktext)';
        if($this->pdo->prepare($this->sql)->execute($data)){
            return true;
        }else{
            throw new \ErrorException('Task did not create');
        }
    }

    public function getTaskData(int $task_id){
        $this->sql = 'SELECT * FROM tasks WHERE id = :task_id';
        $stmt = $this->pdo->prepare($this->sql);
        $stmt->execute([':task_id' => $task_id]);
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $row;
    }

    public function updateTaskData($data){
        $is_modified = $this->checkIsTextModified($data['task_id'], $data['tasktext']);
        if($is_modified){
            $data['modified'] = 1;
        }else{
            $data['modified'] = NULL;
        }

        $data['tasktext'] = htmlspecialchars($data['tasktext']);

        $this->sql = 'UPDATE tasks SET name=:name, email=:email, tasktext=:tasktext, task_done=:task_done, modified=:modified WHERE id=:task_id';
        $stmt = $this->pdo->prepare($this->sql)->execute($data);
        return $stmt;
    }

    public function checkIsTextModified(int $task_id, $text){
        $this->sql = 'SELECT tasktext, modified FROM tasks WHERE id=:task_id';
        $stmt = $this->pdo->prepare($this->sql);
        $stmt->execute([':task_id' => $task_id]);
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);

        if(trim($row['tasktext']) != trim($text) || $row['modified'] == '1'){
            return true;
        }else{
            return false;
        }
    }
}

