<?php
require_once('parts/header.php');
?>
<h1>Tasks</h1>
<div class="tasks-block">
    <div class="filter_head">Filter tasks</div>
    <div class="dropdown">
        <select class="browser-default custom-select" id="filter_by">
            <option value="0" selected>Choose filter name</option>
            <option value="name">User name</option>
            <option value="email">Email</option>
            <option value="task_done">Status</option>
        </select>
        <select class="browser-default custom-select" id="filter_type">
            <option value="0" selected>Chosse filter type</option>
            <option value="ASC">Ascending</option>
            <option value="DESC">Descending</option>
        </select>
        <button class="btn btn-primary" id="filter_tasks">Filter</button>
    </div>
    <?php foreach ($data as $item): ?>
        <div class="card">
            <h5 class="card-title"><?= $item['name']; ?></h5>
            <h6 class="card-subtitle mb-2 text-muted"><?= $item['email']; ?></h6>
            <div class="task_status_wrap">
                <?php if($item['modified'] == '1'){?>
                    <span class="edited">Edited by admin</span>
                <?}?>
                <div>
                    <span>Status:</span>
                    <?php if($item['task_done'] == '1'){?>
                        <span class="done">Task is done</span>
                    <?}?>
                </div>
            </div>
            <p class="card-text">
                <span><?= $item['tasktext']; ?></span>
            </p>
            <?php if(isset($_SESSION['user_id'])){?>
                <p class="edit-link">
                    <a href="/task/edit?task_id=<?=$item['id']?>">Edit task</a>
                </p>
            <?}?>
        </div>
    <?php endforeach; ?>
    <nav aria-label="Page navigation" class="pagination-wrap">
        <ul class="pagination">
            <?php if ($pagination['prev'] != 0) { ?>
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $pagination['prev'] ?><?= $pagination['ordered_params']?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            <? } ?>
            <?php for ($i = 1; $i < $pagination["totalPages"] + 1; $i++) { ?>
                <li class="page-item <? echo ($pagination['page'] == $i) ? 'active" aria-current="page"' : '"' ?> ><a class="
                    page-link" href="?page=<?= $i ?><?= $pagination['ordered_params']?>"><?= $i ?></a></li>
            <?php } ?>
            <?php if ($pagination['next'] <= $pagination["totalPages"]) { ?>
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $pagination['next'] ?><?= $pagination['ordered_params']?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            <? } ?>
        </ul>
    </nav>
</div>
<?php
require_once('parts/footer.php');
?>