<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Bee Jee</a>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="/">Task list <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="/task/create">Create task</a>
            <?php if(isset($_SESSION['user_id'])){?>
                <a class="nav-item nav-link" href="/user/logout">Log out</a>
            <?}else{?>
                <a class="nav-item nav-link" href="/user/login">Log in</a>
            <?}?>
        </div>
    </div>
</nav>
