<?php
    require_once('parts/header.php');
?>

<h1>Create task</h1>
<form id="create-task" action="/task/createtask" method="POST">
    <div class="form-group">
        <label for="InputName">User Name</label>
        <input type="text" name="name" id="name" class="form-control" id="InputName" placeholder="Enter user name">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="email" id="email" class="form-control " id="InputEmail1" placeholder="Enter email">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Task Text</label>
        <textarea class="form-control" id="tasktext" name="tasktext" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<?php
    require_once('parts/footer.php');
?>
