<?php
    require_once('parts/header.php');
?>
<?php if(!empty($_SESSION['user_id'])){ ?>

<h1>Edit task</h1>
<form id="edit-task" action="/task/update" method="POST">
    <div class="form-group">
        <label for="InputName">User Name</label>
        <input type="text" name="name" id="name" class="form-control" id="InputName" placeholder="Enter user name" value="<?=$task_data['name']?>">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="email" id="email" class="form-control " id="InputEmail1" placeholder="Enter email" value="<?=$task_data['email']?>">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Task Text</label>
        <textarea class="form-control" id="tasktext" name="tasktext" rows="3"><?=$task_data['tasktext']?></textarea>
    </div>
    <div class="form-group">
        <div class="custom-control custom-checkbox mr-sm-2">
            <input type="checkbox" name="task_done" class="custom-control-input" id="customControlAutosizing" <? echo ($task_data['task_done'] == '1') ? 'checked="checked"' :'' ?>>
            <label class="custom-control-label" for="customControlAutosizing">Task is done</label>
        </div>
    </div>
        <input type="text" name="task_id" value="<?=$task_data['id']?>" hidden>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php }else{ ?>
    <h1>You should be logged in to edit the task. <a href="/user/login">Log in</a> please.</h1>
<?php } ?>
<?php
    require_once('parts/footer.php');
?>
