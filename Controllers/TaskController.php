<?php


namespace Controllers;


use Models\Tasks;
use System\View;

class TaskController
{
    public function actionCreate(){
        View::render('create-task');
    }

    public function actionCreateTask(){
        $task = new Tasks();
        if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['tasktext'])){
            if($task->insertTask($_POST)){
                return true;
            }
        }
    }

    public function actionEdit(){
        $task = new Tasks();
        $task_data = $task->getTaskData($_GET['task_id']);
        View::render('edit-task',[
            'task_data' => $task_data
        ]);
    }

    public function actionUpdate(){
        $task = new Tasks();
        if($_POST['task_done']){
            $_POST['task_done'] = 1;
        }else{
            $_POST['task_done'] = NULL;
        }
        if(empty($_SESSION['user_id'])){
            return false;
        }

        $task_update = $task->updateTaskData($_POST);
        if($task_update){
            $res = true;
        }else{
            $res = false;
        }

        echo json_encode($res);
    }
}