<?php


namespace Controllers;


use System\View;
use Models\Users;
use System\Sessions;

class UserController
{
    protected $user;
    public function actionLogin(){
        View::render('login');
    }

    public function actionCheckAdmin(){
        $this->user = new Users();
        if($user_info = $this->user->checkUser($_POST)){
            $_SESSION['user_id'] = $user_info['id'];
            $res = true;
        }else{
            $res = false;
        }
        echo json_encode($res);
    }

    public function actionLogOut(){
        $session = new Sessions();
        $session->closeSession();
        header('Location: /');
    }
}