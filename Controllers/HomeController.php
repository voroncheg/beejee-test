<?php

namespace Controllers;

use System\View;
use Models\Tasks;

class HomeController
{
    
    public function actionMain()
    {
        $model = new Tasks();
        
        $data = $model->displayAll($_GET);
        
        View::render('tasks', [
            'data' => $data['result'],
            'pagination' => $data['pagination']
        ]);
    }
}

