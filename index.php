<?php

declare(strict_types=1);

require_once __DIR__ . '/System/autoload.php';
$session = new \System\Sessions();
$session->createSession();
System\App::run();



