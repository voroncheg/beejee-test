$(document).ready(function () {
    $('#register-form').validate({
        rules: {
            name: {
                minlength: 5,
                required: true
            },
            password: {
                required: true
            }
        },
        highlight: function (element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function (form, e) {
            e.preventDefault();
            $.ajax({
                url: '/user/checkadmin',
                type: "POST",
                data: $(form).serialize(),
                dataType: 'json',
                success: function(res) {
                    if(res == true){
                        alert('You are logged as admin');
                        location.reload();
                    }else{
                        alert('Admin credentials are wrong')
                        location.reload();
                    }
                },
                error: function () {
                    alert('Error');
                }
            });
        }
    });

    $('#create-task').validate({
        rules: {
            name: {
                minlength: 5,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            tasktext: {
                minlength: 10,
                required: true
            }
        },
        highlight: function (element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function (form, e) {
            e.preventDefault();
            $.ajax({
                url: '/task/createtask',
                type: "POST",
                data: $(form).serialize(),
                success: function(res) {
                    console.log(res);
                    $("#create-task :input").each(function(){
                        $(this).removeClass('is-valid');
                    });
                    $(form)[0].reset();
                },
                error: function () {
                    alert('Error');
                }
            });
        }

    });

    $('#create-task').validate({
        rules: {
            name: {
                minlength: 5,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            tasktext: {
                minlength: 10,
                required: true
            }
        },
        highlight: function (element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function (form, e) {
            e.preventDefault();
            $.ajax({
                url: '/task/createtask',
                type: "POST",
                data: $(form).serialize(),
                success: function(res) {
                    alert('Task created');
                    $("#create-task :input").each(function(){
                        $(this).removeClass('is-valid');
                    });
                    $(form)[0].reset();
                },
                error: function () {
                    alert('Error');
                }
            });
        }

    });

    $('#edit-task').validate({
        rules: {
            name: {
                minlength: 5,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            tasktext: {
                minlength: 10,
                required: true
            }
        },
        highlight: function (element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function (form, e) {
            e.preventDefault();
            $.ajax({
                url: '/task/update',
                type: "POST",
                data: $(form).serialize(),
                success: function(res) {
                    if(res == 'true'){
                        alert('The task was changed');
                        location.reload();
                    }else{
                        alert('You are not logged in. Log in please.');
                        location.reload();
                    }
                },
                error: function () {
                    alert('Error');
                }
            });
        }
    });

    $('#filter_tasks').on('click',function() {

        let filter_by = $('.dropdown #filter_by').val();
        let filter_type = $('.dropdown #filter_type').val();
        if(filter_by == '0'){
            alert('Choose filter by');
            return;
        }
        if(filter_type == '0'){
            alert('Choose filter type');
            return;
        }

        let current_url = document.location;
        let url = new URL(current_url);
        let page_param = url.searchParams.get("page");
        let page_param_to_url;

        if(page_param){
            page_param_to_url = '?page=' + page_param;
        }else{
            page_param_to_url = '';
        }

        let add_params;

        let n = current_url.href.indexOf('?');

        let separator;

        if(n === -1 || page_param === null){
            separator = '?';
        }else{
            separator = '&';
        }

        add_params = document.location.origin + '/' + page_param_to_url + separator + filter_by + '=' + filter_type;
        document.location = add_params;

    })

});