<?php

namespace System;
use System\Sessions;
class App
{
    
    public static function run()
    {

        $path = parse_url($_SERVER['REQUEST_URI']);
        
        $pathParts = explode('/', $path['path']);
        
        $controller = ucfirst($pathParts[1]);

        if(empty($controller)){
            $controller = 'Home';
        }
        
        $action = $pathParts[2];

        if(empty($action)){
            $action = 'Main';
        }
        
        $controller = 'Controllers\\' . $controller . 'Controller';

        $action = 'action' . ucfirst($action);
  
        if (!class_exists($controller)) {
            throw new \ErrorException('Controller does not exist');
        }

        $objController = new $controller;

        if (!method_exists($objController, $action)) {
            throw new \ErrorException('action does not exist');
        }

        $objController->$action();
    }
}

