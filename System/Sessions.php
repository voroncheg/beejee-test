<?php


namespace System;
use System\Model;

class Sessions extends Model
{
    public $session;

    public function createSession(){
        session_start();
    }

    public function closeSession(){
        session_destroy();
    }
}